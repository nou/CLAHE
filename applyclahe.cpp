#include "applyclahe.h"
#include <QDebug>
#include <opencv2/highgui.hpp>

ApplyCLAHE::ApplyCLAHE()
{
}

void ApplyCLAHE::loadFile(const QString &path)
{
    cv::Mat tmp;
    tmp = cv::imread(path.toLocal8Bit().data(), cv::IMREAD_COLOR | cv::IMREAD_ANYDEPTH);
    int type = tmp.depth();
    _scale = type==CV_8U ? 1.0/255 : 1.0/65535;
    tmp.convertTo(tmp, CV_32F, _scale);
    cv::cvtColor(tmp, tmp, cv::COLOR_BGR2YCrCb);
    cv::split(tmp, _image);
    _image[0].convertTo(_lumImage, type, 1/_scale);
}

void ApplyCLAHE::saveFile(const QString &path)
{
    cv::Mat tmp;
    cv::merge(_image, 3, tmp);
    cv::cvtColor(tmp, tmp, cv::COLOR_YCrCb2BGR);
    tmp.convertTo(tmp, _lumImage.depth(), 1/_scale);
    cv::imwrite(path.toLocal8Bit().data(), tmp);
}

void ApplyCLAHE::apply(float clipLimit, int kernelSize)
{
    cv::Mat lum;
    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE(clipLimit, cv::Size(_lumImage.cols/kernelSize, _lumImage.rows/kernelSize));
    clahe->apply(_lumImage, lum);
    lum.convertTo(_image[0], CV_32F, _scale);
    double min, max;
    cv::minMaxLoc(_image[0], &min, &max);
    qDebug() << min << max << clipLimit << kernelSize;
}

QPixmap ApplyCLAHE::getImage() const
{
    cv::Mat tmp;
    cv::merge(_image, 3, tmp);
    cv::cvtColor(tmp, tmp, cv::COLOR_YCrCb2RGB);
    tmp.convertTo(tmp, CV_8U, 255);
    QImage ret(tmp.data, tmp.cols, tmp.rows, tmp.step1(), QImage::Format_RGB888);
    //ret.bits();//perform deep copy of tmp.data pointer
    return QPixmap::fromImage(ret);
}

QPixmap ApplyCLAHE::getLumImage() const
{
    cv::Mat tmp;
    _image[0].convertTo(tmp, CV_8U, 2.55);
    QImage ret(tmp.data, tmp.cols, tmp.rows, tmp.step1(), QImage::Format_Grayscale8);
    return QPixmap::fromImage(ret);
}

bool ApplyCLAHE::imageLoaded() const
{
    return _lumImage.ptr();
}
