#ifndef APPLYCLAHE_H
#define APPLYCLAHE_H

#include <QImage>
#include <QPixmap>
#include <QString>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

class ApplyCLAHE
{
    cv::Mat _lumImage;
    cv::Mat _image[3];
    double _scale;
public:
    ApplyCLAHE();
    void loadFile(const QString &path);
    void saveFile(const QString &path);
    void apply(float clipLimit, int kernelSize);
    QPixmap getImage() const;
    QPixmap getLumImage() const;
    bool imageLoaded() const;
};

#endif // APPLYCLAHE_H
