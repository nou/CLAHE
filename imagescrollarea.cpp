#include "imagescrollarea.h"
#include <QMouseEvent>
#include <QScrollBar>
#include <QKeyEvent>
#include <QPalette>
#include <QDebug>

ImageScrollArea::ImageScrollArea(QWidget *parent) : QScrollArea(parent),
    m_scale(-1)
{
    m_label = new QLabel(this);
    setWidget(m_label);
    setAlignment(Qt::AlignCenter);
    setBackgroundRole(QPalette::Dark);
}

void ImageScrollArea::setImage(const QPixmap &img)
{
    m_pixmap = img;
    QPixmap pix;
    if(m_scale < 0)
        pix = img.scaled(size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    else
        pix = img.scaled(img.size() * m_scale, Qt::KeepAspectRatio, Qt::SmoothTransformation);

    m_label->setPixmap(pix);
    m_label->resize(pix.size());

    horizontalScrollBar()->setValue(horizontalScrollBar()->maximum() / 2);
    verticalScrollBar()->setValue(verticalScrollBar()->maximum() / 2);
}

void ImageScrollArea::setScale(float scale)
{
    if(scale > 4 || (scale < 0.2 && scale > 0) || m_pixmap.isNull())
        return;

    m_scale = scale;
    QSize newSize = m_scale < 0 ? size() : m_pixmap.size()*scale;
    m_label->setPixmap(m_pixmap.scaled(newSize, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    m_label->resize(newSize);
}

void ImageScrollArea::zoomIn()
{
    if(m_scale < 0)
        m_scale = (float)size().width()/m_pixmap.size().width();

    setScale(m_scale + 0.1);
}

void ImageScrollArea::zoomOut()
{
    if(m_scale < 0)
        m_scale = (float)size().width()/m_pixmap.size().width();

    setScale(m_scale - 0.1);
}

void ImageScrollArea::bestFit()
{
    setScale(-1);
}

void ImageScrollArea::oneToOne()
{
    setScale(1);
}

void ImageScrollArea::keyPressEvent(QKeyEvent *event)
{
    event->ignore();
}

void ImageScrollArea::keyReleaseEvent(QKeyEvent *event)
{
    event->ignore();
}

void ImageScrollArea::mouseMoveEvent(QMouseEvent *event)
{
    QPoint delta = m_lastPos - event->pos();
    horizontalScrollBar()->setValue(horizontalScrollBar()->value() + delta.x());
    verticalScrollBar()->setValue(verticalScrollBar()->value() + delta.y());
    m_lastPos = event->pos();
}

void ImageScrollArea::mousePressEvent(QMouseEvent *event)
{
    m_lastPos = event->pos();
}

void ImageScrollArea::resizeEvent(QResizeEvent *event)
{
    if(m_scale < 0 && !m_pixmap.isNull())
    {
        m_label->setPixmap(m_pixmap.scaled(event->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
        m_label->resize(event->size());
    }
    QScrollArea::resizeEvent(event);
}

void ImageScrollArea::wheelEvent(QWheelEvent *event)
{
    if(m_scale < 0)
        m_scale = (float)size().width()/m_pixmap.size().width();

    QPointF top(horizontalScrollBar()->value(), verticalScrollBar()->value());
    QPointF mousePos = (top + event->posF()) / m_scale;

    QPoint delta = event->angleDelta();
    if(delta.y() > 0)
        setScale(m_scale + 0.1);
    else
        setScale(m_scale - 0.1);

    mousePos *= m_scale;
    top = mousePos - event->posF();
    horizontalScrollBar()->setValue(top.x());
    verticalScrollBar()->setValue(top.y());
}
