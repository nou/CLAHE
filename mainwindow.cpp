#include "mainwindow.h"
#include <QStandardPaths>
#include <QFileDialog>
#include <QMenuBar>
#include <QPushButton>
#include <QLineEdit>
#include <QDockWidget>
#include <QHBoxLayout>
#include "applyclahe.h"

const QString imageFilter = "Images (*.png *.jpg *.jpeg);;All files (*)";

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    setupUI();
    _clahe = new ApplyCLAHE;
}

MainWindow::~MainWindow()
{
    delete _clahe;
}

void MainWindow::openFile()
{
    QStringList list = QStandardPaths::standardLocations(QStandardPaths::StandardLocation::PicturesLocation);
    list.append("");
    QString path = QFileDialog::getOpenFileName(this, tr("Open image"), list.first(), imageFilter);
    if(!path.isEmpty())
    {
        _clahe->loadFile(path);
        applyClahe();
    }
}

void MainWindow::saveFile()
{
    QStringList list = QStandardPaths::standardLocations(QStandardPaths::StandardLocation::PicturesLocation);
    QString path = QFileDialog::getSaveFileName(this, tr("Save image"), list.first(), imageFilter);
    if(!path.isEmpty())
    {
        _clahe->saveFile(path);
    }
}

void MainWindow::applyClahe()
{
    if(_clahe->imageLoaded())
    {
        QPixmap pixmap;
        _clahe->apply(_clipLimitSlider->value()/100.0f, _kernelSizeSelect->currentText().toInt());
        pixmap = _clahe->getImage();
        _imageScrollArea->setImage(pixmap);
    }
}

void MainWindow::applyLive()
{
    if(_liveApply->isChecked())
        applyClahe();
}

void MainWindow::setupUI()
{
    resize(1024, 768);

    QMenu *fileMenu = menuBar()->addMenu(tr("File"));
    fileMenu->addAction(tr("Open file"), this, SLOT(openFile()), QKeySequence("Ctrl+O"));
    fileMenu->addAction(tr("Save file"), this, SLOT(saveFile()), QKeySequence("Ctrl+S"));
    _liveApply = fileMenu->addAction(tr("Live mode"));
    _liveApply->setCheckable(true);
    _liveApply->setChecked(true);
    fileMenu->addAction(tr("Quit"), this, SLOT(close()), QKeySequence("Ctrl+Q"));

    _clipLimitEdit  = new QLineEdit;
    _clipLimitEdit->setInputMask("000.00");
    _clipLimitEdit->setText("1.00");
    _clipLimitEdit->setMaximumWidth(100);

    _clipLimitSlider = new QSlider(Qt::Horizontal);
    _clipLimitSlider->setRange(0, 10000);
    _clipLimitSlider->setValue(100);
    connect(_clipLimitSlider, SIGNAL(valueChanged(int)), this, SLOT(clipSliderValueChanged(int)));

    QPushButton *applyButton = new QPushButton(tr("Apply"));
    connect(applyButton, SIGNAL(clicked()), this, SLOT(applyClahe()));

    _kernelSizeSelect = new QComboBox;
    _kernelSizeSelect->addItems({"8", "12", "16", "24", "32", "64", "128", "256"});
    connect(_kernelSizeSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(applyLive()));

    QHBoxLayout *claheLayout = new QHBoxLayout;
    claheLayout->addWidget(_clipLimitEdit);
    claheLayout->addWidget(_clipLimitSlider);
    claheLayout->addWidget(_kernelSizeSelect);
    claheLayout->addWidget(applyButton);

    QDockWidget *claheDock = new QDockWidget(tr("CLAHE"), this);
    QWidget *claheWidget = new QWidget(claheDock);
    claheDock->setWidget(claheWidget);
    claheWidget->setLayout(claheLayout);
    addDockWidget(Qt::TopDockWidgetArea, claheDock);

    _imageScrollArea = new ImageScrollArea(this);
    setCentralWidget(_imageScrollArea);
}

void MainWindow::clipSliderValueChanged(int value)
{
    _clipLimitEdit->setText(QString::number(value/100.0));
    applyLive();
}
