#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QComboBox>
#include "applyclahe.h"
#include "imagescrollarea.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QComboBox *_kernelSizeSelect;
    QSlider *_clipLimitSlider;
    QLineEdit *_clipLimitEdit;
    ApplyCLAHE *_clahe;
    ImageScrollArea *_imageScrollArea;
    QAction *_liveApply;
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void openFile();
    void saveFile();
    void applyClahe();
    void applyLive();
private:
    void setupUI();
private slots:
    void clipSliderValueChanged(int value);
};

#endif // MAINWINDOW_H
